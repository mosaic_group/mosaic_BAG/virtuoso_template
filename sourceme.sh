# Sourceme template for a BAG2 project

if [ "$PWD" == "$HOME" ]; then
 echo "Do not run this script in your home directory."
 echo "Configuration files in home directory may mess up your configuration globally."
 return 1
fi

# Collect environment variables to the beginnning of the file
# PDK_HOME and TECHLIB are ued by BAG2 YOU MUST DEFINE THEM
# TECHLIB_DRC is needed for DRC runs.
#export PDK_HOME="Path to your PDK home"
#export TECHLIB="Name of the virtuoso technology library"
#export TECHDIR_DRC="PATH to the directory containing DRC techfiles"

##Copy essential files to the project directory
## I do not know where you have them, but you must provide them
## Example:
##set FILELIST=".cdsinit CCSinvokeCdfCallbacks.il"
##set FILESOURCE="This is from where you can copy the necessary files to this directory"
##foreach file ( ${FILELIST} )  
##  if ( ! -f $file ) then
##    echo "Copying file ${FILESOURCE}/${file} to ./${file}"
##    cp ${FILESOURCE}/${file} ./
##  else if (  !="" ) then
##    echo "System wide ${FILESOURCE}/${file} is newer than ./${file}"
##    echo "Please check your configuration *** "
##    echo "Please check your configuration *** "
##    echo "Please check your configuration"
##  endif
##end
#
#### Here I will do some checks for you so you do not screw up
if [ ! $?PDK_HOME ]; then
    echo "PDK_HOME not set"
    echo "In this file you MUST set your environment setup so that you have all tools available and PDK set up correctly"
    echo "It is NOT taken care for you by this file"
    echo "Exiting"
    return
fi

if [ ! $?TECHLIB ]; then
    echo "TECHLIB not set"
    echo "It is NOT taken care for you by this file"
    echo "Exiting"
    return
fi

if [ ! $?TECHDIR_DRC ]; then
    echo "NOTE: TECHDIR_DRC not set"
    echo "Generator DRC runs with 'make drc' will fail"
fi


if [  -z "`command -v virtuoso`" ]; then
    echo "FATAL: Virtuoso not in path"
    echo "Exiting"
    return
fi

if [ ! -f ./cds.lib.core ]; then
    echo "You must provide cds.lib.core"
    echo "cds.lib.core should define general and PDK libraries"
    echo "Exiting"
    return
fi

if [ ! -f ./.cdsinit ]; then
    echo "You must provide ./.cdsinit"
    echo "Usually provided by your PDK vendor"
    echo "Exiting"
    return
fi

if [ ! -f ./cds.lib ]; then
    echo "You do not have  ./.cds.lib"
    echo "I will create one for you"
cat << EOF > ./cds.lib
SOFTINCLUDE ./cds.lib.bag
SOFTINCLUDE ./cds.lib.core
EOF

fi


## Softinclude BAG2 structured library definitions
if [ "`grep "SOFTINCLUDE ./cds.lib.core" ./cds.lib`" == "" ]; then
    echo "Adding cds.lib.core to cds.lib"
    sed -i '1s#^#SOFTINCLUDE ./cds.lib.core\n#' ./cds.lib 
fi

if [ "`grep "SOFTINCLUDE ./cds.lib.bag" ./cds.lib`" == "" ]; then
    echo "Adding cds.lib.bag to cds.lib"
    sed -i '1s#^#SOFTINCLUDE ./cds.lib.bag\n#' ./cds.lib 
fi

# Check for user customization section in .cdsinit
if [ "`grep ";END OF USER CUSTOMIZATION" ./.cdsinit`" == "" ]; then
    chmod 750 ./.cdsinit
    echo ";END OF USER CUSTOMIZATION" >> ./.cdsinit
fi

if [ "`grep 'load("start_bag.il")' ./.cdsinit`" == "" ]; then
    echo "Adding start_bag.il to .cdsinit"
    sed -i 's/\(^ *;END OF USER CUSTOMIZATION *$\)/load\("start_bag.il"\)\n\1/g' ./.cdsinit
fi

if [ ! -f ./CCSinvokeCdfCallbacks.il ];then
    echo "You do not have CCSinvokeCdfCallbacks.il"
    echo "It is mandatory if you use parametrized primitives with BAG"
    echo "Download it from here:" 
    echo "https://support.cadence.com/apex/ArticleAttachmentPortal?id=a1Od0000000namyEAA&pageName=ArticleContent"
    return
fi

if [ "`grep 'load("CCSinvokeCdfCallbacks.il")' ./.cdsinit`" == "" ]; then
    echo "Adding CCSinvokeCdfCallbacks.il to .cdsinit"
    sed -i 's/\(^ *;END OF USER CUSTOMIZATION *$\)/load\("CCSinvokeCdfCallbacks.il"\)\n\1/g' ./.cdsinit
fi

#### This might be needed
###if ( -z $(grep CCSCdfCallbackEntireLib.il ./.cdsinit) ) then
###    sed -i 's/\(^ *;END OF USER CUSTOMIZATION *$\)/load\("CCSCdfCallbackEntireLib.il"\)\n\1/g' ./.cdsinit
###endif
##

#############################################################################
# NOTE: BAG_WORK_DIR, VIRTUOSO_DIR and BAG_GENERATOR_ROOT
# are decoupled for flexible definition of locations
# set BAG root directory 
#############################################################################

# Defaults to virtuoso_template root
export BAG_WORK_DIR="$PWD"  
# Virtuoso run directory from which you launch the builds 
# containing "working" artefacts, such as log files and the BAG_server_port.txt, 
# etc)
export VIRTUOSO_DIR="$BAG_WORK_DIR"
# Root directory containing generators
export BAG_GENERATOR_ROOT=${BAG_WORK_DIR}

# set BAG framework directory
export BAG_FRAMEWORK="${BAG_WORK_DIR}/BAG2_framework"
# set BAG python executable
export BAG_PYTHON="python3"
# set jupyter notebook path
#export BAG_JUPYTER "${HOME}/.local/bin/jupyter-notebook"
# set technology/project configuration directory
export BAG_TECH_CONFIG_DIR="${BAG_WORK_DIR}/BAG2_technology_definition"
# Directory for LVS definition files
export LVS_FILES_DIR="${BAG_TECH_CONFIG_DIR}/lvs_files"
# Directory for DRC definition files
export DRC_FILES_DIR="${BAG_TECH_CONFIG_DIR}/drc_files"
# Directory for PEX definition files
export PEX_FILES_DIR="${BAG_TECH_CONFIG_DIR}/pex_files"
# set where BAG saves temporary files
export BAG_TEMP_DIR="${VIRTUOSO_DIR}/BAGTMP"
# set location of BAG configuration file
export BAG_CONFIG_PATH="${BAG_WORK_DIR}/bag_config.yaml"

## set IPython configuration directory
##export IPYTHONDIR "$BAG_WORK_DIR/.ipython"

# This takes ecd_bag helpers into use
if [ ! $?PYTHONPATH ]; then
    export PYTHONPATH="${BAG_WORK_DIR}:${BAG_WORK_DIR}/bag_ecd:${BAG_WORK_DIR}/BAG2_methods"
else
    export PYTHONPATH="${PYTHONPATH}:${BAG_WORK_DIR}:${BAG_WORK_DIR}/bag_ecd:${BAG_WORK_DIR}/BAG2_methods"
fi

if [ ! -f ./bag_libs.def ]; then
    echo "./bag_libs.def does not exist. Creating it for you"
    echo 'BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives'  > ./bag_libs.def
fi

echo "Run 'virtuoso'"
echo "To configure bag project for compilations, run ./configure"


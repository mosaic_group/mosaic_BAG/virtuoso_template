#!/usr/bin/env bash
#############################################################################
# This script updates all submodules to the HEAD af the given branch name.
# Usefull for keeping the submodules up to date at the HEADs of the release 
# candidate branches.
# OBS: Assumes that submodules are tested before push to the mentioned branch. 
#
# Created by Marko Kosunen on 15.10.2022
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="release_candidate_update.sh"
cat << EOF
${SCRIPTNAME} Release 1.0 (15.10.2022)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-update submodule release candidates"
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Updates the submodule HEAD to the HEAD of the given branch.
    Module names extracted from init_submodules in the current directory.

OPTIONS
  -b
      Branch name. Default: The last branch ending _RC when ordered alphanumerically 
      from beginning of the word. i.e 10 comes before 1. 
  -P
      Allow automatic update of BAG2_technology_definition.
  -h
      Show this help.
EOF
}

THISDIR="$(pwd)" 
# Release candidate branch
RC="$(git branch -a \
    | sed -n '/remotes/p' \
    | sed -n '/_RC$/p' \
    | sed -n 's#.*/\(.*_RC$\)#\1#p' \
    | sort -r \
    | head -n1 \
    )"
ANS=""
while getopts b:Ph opt
do
  case "$opt" in
    b) RC=$OPTARG;;
    P) ANS="no";;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

#Modules extracted from init_submodules
MODULES="$(sed -n '/MODULES="/,/"/p' init_submodules.sh \
    | sed -e '/MODULES/d' -e 's/["]//g' -e s'/\\//g' -e 's/\s\s*//g' \
    | xargs \
    )"

#Self-made selective recursion
for mod in $MODULES; do
    git submodule update --init
    if [ -f $mod/init_submodules.sh ]; then
        cd $mod
        ./init_submodules.sh
    fi
done

# Pull the latest ones
for mod in $MODULES; do
    cd $mod
    RCTEST="$(git branch -a | grep "remotes/origin/${RC}" | sed 's/\s\s*//g')"
    if [ "$mod" == "BAG2_technology_definition" ] && [ -z "$ANS" ]; then
        echo "Do you really wish to update $mod? [ no | yes ]"
        read ANS
    fi
    if [ ! -z "$RCTEST" ] && [ "$mod" == "BAG2_technology_definition" ] && [ "$ANS" == "yes" ]; then
        echo "Updating $mod"
        git checkout $RC 
        git pull
        cd $THISDIR
        git add $mod && git commit -m"Update $mod to the HEAD of $RC"
        ANS="no"
    elif [ ! -z "$RCTEST" ] && [ "$mod" != "BAG2_technology_definition" ]; then
        echo "Updating $mod"
        git checkout $RC 
        git pull
        cd $THISDIR
        git add $mod && git commit -m"Update $mod to the HEAD of $RC"
    else
        echo "No update for $mod"
        cd $THISDIR
    fi
done

exit 0


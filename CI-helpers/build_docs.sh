#!/usr/bin/env bash
######################################################################
# Documentation build script for CI-pipeline
#
# Initially written by Marko Kosunen, marko .kosunen@aalto.fi, 16.4.2022
######################################################################

#Function to display help with -h argument and to control
#the configuration from the command line
help_f()
{
SCRIPTNAME="build_docs.sh"
cat << EOF
${SCRIPTNAME} Release 1.0 (16.4.2022)

Written by Marko Kosunen

SYNOPSIS
    $(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:]) [OPTIONS]
DESCRIPTION
  Compiles docstrings documentation for a mosaic_BAG module that has a proper 
  documentation sphinx-build Makefile strored in a directory 'doc'.

OPTIONS
  -c
      If given, will assume that the script is executed as part of CI 
      process.
  -e
      Export local variables to environment variables
  -m  
      Module directory. Default <current directory>/doc
  -v
      Virtuoso directory. Default current directory
  -h
      Show this help.
EOF
}


# We assume that this script is called from the root of the virtuoso_template
VIRTUOSO_DIR="$(pwd)"
MODULEDIR="$(pwd)"
EXPORT='0'
ISCI='0'

while getopts cem:v:h opt
do
  case "$opt" in
    c) ISCI='1';;
    e) EXPORT='1';;
    m) MODULEDIR=${OPTARG};;
    v) VIRTUOSO_DIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

PATH=${PATH}:${HOME}/.local/bin
PYTHONPATH=${VIRTUOSO_DIR}/:${VIRTUOSO_DIR}/bag_ecd:${VIRTUOSO_DIR}/BAG2_framework
PYTHONPATH=${PYTHONPATH}:${VIRTUOSO_DIR}/BAG2_templates

BAG_WORK_DIR=${VIRTUOSO_DIR}
BAG_FRAMEWORK=${VIRTUOSO_DIR}/BAG2_framework
BAG_TECH_CONFIG_DIR=${BAG_WORK_DIR}/BAG2_technology_definition
BAG_CONFIG_PATH=${BAG_WORK_DIR}/bag_config.yaml


if [ "${EXPORT}" == '1' ] || [ "${ISCI}" == '1' ]  ; then
    export VIRTUOSO_DIR
    export PATH
    export PYTHONPATH
    export PYTHONPATH
    
    export BAG_WORK_DIR
    export BAG_FRAMEWORK
    export BAG_TECH_CONFIG_DIR
    export BAG_CONFIG_PATH
fi

if [ "${ISCI}" == '1' ]  ; then
    ${VIRTUOSO_DIR}/CI-helpers/clone-recursive-https.sh ${VIRTUOSO_DIR}
    if [ -z "${CI_PROJECT_DIR}" ]; then
        echo "CI_PROJECT_DIR not defined. Please set it as an environment variable"
        exit 1
    fi
else
    CI_PROJECT_DIR="${VIRTUOSO_DIR}"
fi
${VIRTUOSO_DIR}/init_submodules.sh

mkdir ${HOME}/.local
mkdir ${HOME}/.local/bin
${VIRTUOSO_DIR}/pip3userinstall.sh

rm -rf $CI_PROJECT_DIR/public
cd ${MODULEDIR}/doc/ && make html && mv ./build/html $CI_PROJECT_DIR/public

exit 0




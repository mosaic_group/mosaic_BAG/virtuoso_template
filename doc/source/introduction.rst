=============================================
Introduction to MOSAIC BAG2 virtuoso template
=============================================

Modular Open Source Analog Integrated Circuits Berkeley Analog Generator
is a development fork of original BAG2 circuit design environment 
developed at University of California, Berkeley.

Objective of this for is to provide a low entry threshod to BAG2 by 
providing a documented easy-to-setup minimum working example of a 
BAG2 desing project. The template id build onthe following scripts and modules

Template organization
=====================

Scripts
-------

In order of execution after cloning

#. init_submodules.sh 

   * initializes all the submoudles of the project


#. sourceme.csh

   * C-shell to be sourced to set up your desing environment the pre-requisite is that you have a *working* virtuoso environment set up before you source this script.


#. configure

   * script that is used to generate a Makefile. The philosophy of *every*

   * compilation is to  run `configure && make <recipe>`



Submodules
----------

Submodules that are documented separately are listed below. Use of 
submodules makes it possible to separate but still enable the 
development  of thid environment from your projects. Updates to methodology can be propagated to 
projects selectively through submodule updates.

* BAG2_environment settings

   * Used to provide environment specific setting files

* BAG2_framework

   * BAG2 core framework forked form UC Berkeley

* BAG2_templates

   * Submodule containing design templates for AnalogBAse 

* BAG2_tecnology_templates

   * Contains abstract classes for process parameter and method setups for BAG2. These classes are used in BAG2_technology_definition as parent classes to orce the implementation of essential process definition parameters. Parameters are documented in this module.

* BAG2_methods

   * A module that enables easy-to-use generator framework for circuit generators.  With this methodology, every generator has an identical structure of main class, schematic class and layout class, and by following this strucuture all the generators are compatible and can be used in constructions of other generators. Contains also helpers for routing and placement. This is the module where all generic helper functions are collected.

* inverter_gen

   * Minimum working example of an circuit generator.


Project status and releases
===========================

Release schedule
----------------

MOSAIC BAG has bi-annual releases currenly implemented of virtuoso_template project. The release dates are 4.4 and 10.10.

**Roadmap for next release**

* Separate simulation methodology from generator methodology


**A bit longer term roadmap**

* support for open source tool

**Release 1.2 (18.11.2023)**

* Python version change to 3.10

* BAG2_templates improvements

  * Support for thick oxide transistor rows
  * Improvements to dummy transistor connnection handling

**Release 1.1 (16.4.2023)**

* Shell scripts for DRC and LVS

* BAG2_framework improvements

   *  DRC support
   *  RCX: Fixes and error handling improvements
   *  PVS checker: added command options to support lvsbrowser tool
   *  Logging

   * Improved BAG server related logging Flow subprocesses: Added logging of command lines, log file names and return codes


   * BAG server

        * Allow virt_server.sh to pass a single port range (i.e. 6000-6000 instead of 5000-9999) 
          Fixed an issue where a second simulation causes the launch of a second BAG server,  
          while the previous server port was still reachable


* Netlist extraction phase: Allow injection of a custom BagModules script renderer


* BAG2_methods/sal improvements

    * Common Generator Tool (gen script):

    * DRC support
    * RCX support
    * Simulation support
    * Failed LVS/DRC will open a GUI report in case of errors inject custom BagModules script 
      renderer for generator/testbench packages, which directly generates the correct script, 
      therefore getting rid of the necessity to refresh Virtuoso
    * Helper classes for leaner manual routing
    * Validation tool for ABC interface compliance of the currently active BAG2_technology_definition

**Release 1.0 (10.10.2022)**

* Minimum working example projet template and generator

* Class-based process configuration methodology

* Self guiding setup scripts

* CI/CD build for documentation

* Policy for contributions

